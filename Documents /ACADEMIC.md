# **Academic Writing**

 *[From Wikipedia](https://en.wikipedia.org/wiki/Academic_writing)*
 
 **Academic writing** is conducted in several sets of forms and [genres](https://en.wikipedia.org/wiki/Genre), normally in an impersonal and dispassionate tone, targeted for a critical and informed audience, based on closely investigated knowledge, and intended to reinforce or challenge concepts or arguments. It usually circulates within the academic world ('the academy'), but the academic writer may also find an audience outside via journalism, speeches, pamphlets, etc. Typically, scholarly writing has an objective stance, clearly states the significance of the topic, and is organized with adequate detail so that other scholars may try to replicate the results. Strong papers are not overly general and correctly utilize formal academic rhetoric. <br/>
 
 ![Academic Writing](https://lnu.se/ImageVault/publishedmedia/oixm5lq7c8886adf9nih/Hand_skriver.jpg =350x220)
 
 This article provides a short summary of the full spectrum of critical and academic writing and lists the genres of academic writing. It does cover the variety of critical approaches that can be applied when one writes about a subject. However, as Harwood and Hadley (2004) and Hyland (2004) have pointed out, the amount of variation that exists between different disciplines may mean that we cannot refer to a single academic literacy.[^note] While academic writing consists of a number of text types and genres, what they have in common, the conventions that [academic](https://en.wikipedia.org/wiki/Academy) writers traditionally follow, has been a subject of debate. Many writers have called for conventions to be challenged, for example Pennycook (1997) and Ivanic (1998), while others suggest that some conventions should be maintained, for example Clark (1997, p136).

[^note]: Stephen Catterall; Christopher Ireland (October 2010). ["Developing Writing Skills for International Students: Adopting a critical pragmatic approach"](https://www.researchgate.net/publication/269872663_Developing_Writing_Skills_for_International_Students_Adopting_a_critical_pragmatic_approach). ResearchGate. 

 
## Discourse community

A discourse community is essentially a group of people that shares mutual interests and beliefs. "It establishes limits and regularities...who may speak, what may be spoken, and how it is to be said; in addition [rules] prescribe what is true and false, what is reasonable and what foolish, and what is meant and what not." (Porter, 39). People are generally involved in a variety of discourse communities within their private, social, and professional lives. Some discourse communities are very formal with well established boundaries, while others may have a more loose construction with greater freedom. Additionally, discourse communities have approved channels of communication in which members write or speak through. These channels can be a web page, a journal, a blog, or any other medium people use to communicate through. Examples of discourse communities may include but certainly not limited to:

- [x] Medicine
- [x] Economics (theory and applied)
- [x] Law
- [x] Psychology
- [x] Films (Movies)
- [x] General Forums
- [x] Technology
- [x] Sociology
- [x] Philosophy
- [x] Chemistry
- [x] Physics
- [x] Mathematics
- [x] Writing
- [x] Rhetoric and Composition

The concept of a discourse community is vital to academic writers across nearly all disciplines, for the academic writer's purpose is to influence a discourse community to think differently. At the same time the discourse community does not expect to see any writing that appears too foreign. For this reason the academic writer must follow the constraints (see article section below) set by the discourse community so his or her ideas earn approval and respect.
 
## Writing for a discourse community

In order for a writer to become familiar with some of the constraints of the discourse community they are writing for, a useful tool for the academic writer is to analyze prior work from the discourse community. The writer should look at the textual 'moves' in these papers, focusing on how they are constructed. Across most discourses communities, writers will:

* Identify the novelty of their position
* Make a claim, or thesis
* Acknowledge prior work and situate their claim in a disciplinary context
* Offer warrants for one's view based on community-specific arguments and procedures (Hyland)

Each of the 'moves' listed above are constructed differently depending on the discourse community the writer is in. For example, the way a claim is made in a high school paper would look very different from the way a claim is made in a college composition class. [^note] It is important for the academic writer to familiarize himself or herself with the conventions of the discourse community by reading and analyzing other works, so that the writer is best able to communicate his or her ideas. (Porter) Contrary to some beliefs, this is by no means plagiarism.

Writers should also be aware of other ways in which the discourse community shapes their writing. Other functions of the discourse community include determining what makes a novel argument and what a 'fact' is. The following sections elaborate on these functions.

[^note]: Wikidot: Academic Writing For the Students Academic Writing

