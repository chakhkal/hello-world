# Project Title 

A little info about your [project](https://gitlab.com/chakhkal/hello-world) and/ or overview that explains **what** the project is about.

## The Project Purpose 

Potential users of your project should be able to figure out quickly what the purpose of the project is. Make sure to get this information across early on! A good way to do this right is by providing:

* a concise, single-paragraph blurb describing your project; and
* a representative screenshot (or even better, an animated GIF) that shows your project in action.


![alt text](https://gitlab.com/chakhkal/hello-world/raw/master/Documents%20/logo.png)

## Build Status

[![pipeline status](https://gitlab.com/chakhkal/hello-world/badges/master/pipeline.svg)](https://gitlab.com/chakhkal/hello-world/commits/master)

## Screenshots

![Screenshot1](https://gitlab.com/chakhkal/hello-world/raw/master/Documents%20/Project_Gitlab.png)

*General [Project](https://gitlab.com/chakhkal/hello-world) Overview*

![Screenshot2](https://gitlab.com/chakhkal/hello-world/raw/master/Documents%20/Test_Wiki.png)

*What this project is about [Wiki](https://gitlab.com/chakhkal/hello-world/wikis/home) information*

## Programming Languages Used

* [HTML](https://gitlab.com/chakhkal/hello-world/blob/master/index.html%20)
* ~~PHP~~
* Ruby

**Code Examples:** [^1]

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="project" content="GitLab Test">
    <title>HTML Test</title>
  </head>
  <body>

```

## Tech/Framework/Libraries Used 

**Build with:** 

* `.gitlab-ci.yml`
* [Electron](https://electronjs.org)


An example of `.gitlab-ci.yml` configuration:

```
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
  
```
## Features

* Highlighting the best features
* Why the project is standing out
* ``Adds Value``
* ~~Why the reader needs it~~
* *Always updates*

**Other Examples:** [^2]

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D
  D-->A
  
```

*************************

**Release 2.1**

- [x] March 2018

- {+ Added these feature +}
- [+ Another one +]
- {- Was deleted -}

- [x] Color updates used

`#F00AAA`
`#FFF000`

*************************

## Installation

1. Install **GitLab**.
2. Use `New Project` to create a new GitLab Project.
3. Add this `.gitlab-ci.yml` to the root of your project.
4. Push your repository and changes to the `master` branch.


## API Reference

**API** requests should be prefixed with api and the **API version**. The API version is defined in `lib/api.rb`. 
For example, the root of the `v4 API is` at `/api/v4`.

Example of a valid `API request using cURL`:

```curl "https://gitlab.example.com/api/v4/projects"```

The **API** uses **JSON** to serialize data. You don't need to specify .json at the end of an **API URL**.

## Tests

>How to run the tests with code examples.

1. Fork this repository.
1. **IMPORTANT:** Remove the fork relationship.
Go to **Settings (⚙)** > **Edit Project** and click the **"Remove fork relationship"** button.

## How to use?

1. Install `Software`.
2. Use **GitLab New Project** to create a new GitLab Project.
3. Add this `.gitlab-ci.yml` to the root of your project.
4. Push your repository and changes to the `master` branch.


## Contribute

Contributions are always welcome! Please read the **[contribution guidelines](https://gitlab.com/chakhkal/hello-world/blob/master/CONTRIBUTING.md)** first.

## Credits

:family:  

 **Commits** | **Contributor**<br/> | 
| --- | --- |
| 20 | Yghsghs |
| 14 | Afdgsgs |
| 5  | Ghvsdvbs|

- *First Name last Name - Contributed to this [Project](https://gitlab.com/chakhkal/hello-world/tree/master/Markdown)*

- *Interesting informaiton on [GitLab Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)*


## License


[MIT License](https://gitlab.com/chakhkal/hello-world/blob/master/LICENSE)

MIT ©Varduhi Chakhkalyan


[^1]: As an example 
[^2]: Another exmaple



