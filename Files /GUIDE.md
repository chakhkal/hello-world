# **An ``Ultimate`` Guide with Markdown**

From Wikipedia <br/>

*For other uses, see [Guide (disambiguation)](https://en.wikipedia.org/wiki/Guide_(disambiguation)).*

A **guide** is a person who leads travelers or tourists through unknown or unfamiliar locations. The term can also be applied to a person who leads others to more abstract goals such as [knowledge](https://en.wikipedia.org/wiki/Knowledge) or [wisdom](https://en.wikipedia.org/wiki/Wisdom).

![Australian mountain guides](https://upload.wikimedia.org/wikipedia/commons/3/3f/Anselm_Klotz%28L%29-Josef_Frey%28R%29.jpg)

*The austrian mountaineers and guides **Anselm Klotz** (on the left) and **Josef Frey** (on the right), who climbed de:Freispitze the first time.*

### **Contents** 
1. Travel and recreation

   1.1 Tour guide   
   1.2	Mountain guide    
   1.3	Wilderness guide  
   1.4	Hunting guide     
   1.5	Safari guide      
   1.6	Fishing guide*   

## **Travel and recreation**

---------------------------------------------------------------

**Explorers** in the past venturing into territory unknown by their own people invariably hired guides. Lewis and Clark hired Sacagawea to help them explore the American West, and [Wilfred Thesiger](https://en.wikipedia.org/wiki/Wilfred_Thesiger) hired guides in the deserts that he ventured into, such as Kuri on his journey to the [Tibesti](https://en.wikipedia.org/wiki/Tibesti_Mountains) Mountains in 1938.
  
### **Tour guide**

*Main article:* *[Tour guide](https://en.wikipedia.org/wiki/Tour_guide)*

A tour guide at the Centre Block in Canada.
Tour guides lead visitors through [tourist attractions](https://en.wikipedia.org/wiki/Tourist_attraction) and give information about the attractions' natural and cultural significance. Often, they also act as interpreters for travelers who do not speak the local language. Automated systems like audio tours are sometimes substituted for human tour guides. [Tour operators](https://en.wikipedia.org/wiki/Tour_operator) often hire guides to lead tourist groups.

![Tour Guides](https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Tourguide2.jpg/800px-Tourguide2.jpg) <br/>

*A tour guide at the [Centre Block](https://en.wikipedia.org/wiki/Centre_Block) in Canada.*

### **Mountain guide**

*Main article:* *[Mountain Guide](https://en.wikipedia.org/wiki/Mountain_guide)*

Mountain guides are those employed in [mountaineering](https://en.wikipedia.org/wiki/Mountaineering); these are not merely to show the way but stand in the position of professional climbers with an expert knowledge of rock and snowcraft, which they impart to the amateur, at the same time assuring the safety of the climbing party. This professional class of guides arose in the middle of the 19th century when Alpine climbing became recognized as a [sport](https://en.wikipedia.org/wiki/Sport). 

In Switzerland, the central committee of the Swiss Alpine Club issues a guides’ tariff which fixes the charges for guides and porters; there are three sections, for the Valais and Vaudois Alps, for the [Bernese Oberland](https://en.wikipedia.org/wiki/Bernese_Highlands), and for central and eastern Switzerland.

In [Chamonix (France)](https://en.wikipedia.org/wiki/France) a statue has been raised to Jacques Balmat, who was the first to climb Mont Blanc in 1786. Other notable European guides are Christian Almer, Jakob and Melchior Anderegg, Auguste Balmat, Alexander Burgener, Armand Charlet, Michel Croz, François Devouassoud, Angelo Dibona, Andreas Heckmair, the Innerkofler family, Conrad Kain, Christian Klucker, and Matthias Zurbriggen.

### **Wilderness guide**

A [wilderness guide](https://en.wikipedia.org/wiki/Wilderness) leads others through wilderness areas and works to ensure the safety of their clients.

![Wilderness Guide](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Raquette_River_-_at_Sweeney_Carry.jpg/220px-Raquette_River_-_at_Sweeney_Carry.jpg)

Wilderness guides are expected to have a command of survival skills (such as making shelters, fire-making, navigation, and first aid) and an understanding of the [ecology](https://en.wikipedia.org/wiki/Ecology) and history of the location where they guide. Other common skills among guides include traditional handicrafts and cooking methods, fishing, hunting, bird watching, and nature [conservation](https://en.wikipedia.org/wiki/Conservation_(ethic)).

Wilderness tours usually take place on foot (or skis or snowshoes if there is snow) but may also involve other vehicles such as cars, [snowmobiles](https://en.wikipedia.org/wiki/Snowmobile), canoes, kayaks, or sledges.

### **Hunting guide**

Hunting guides are employed by those seeking to hunt wildlife, especially big game animals in the wild. European hunting guides working in Africa are sometimes called [white hunters](https://en.wikipedia.org/wiki/White_hunter), although the term is most commonly used in the context of the early 20th century.

### **Safari guide**

Guides employed on [safari](https://en.wikipedia.org/wiki/Safari), usually for "photographic safaris", although the term can also be a synonym of hunting guide. Safari guides who are self-employed, working on their own account with their own marketing and clientele (in contrast with those who work for an employer) sometimes refer to themselves as "professional safari guides". Safari Guides can be unqualified, but most should be qualified and be part of an Association. Associations are typically linked to specific countries and are governed by that countries laws and policies. Associations such as The Field Guides Association of Southern Africa (FGASA) and Uganda Safari Guides Association (USAGA) play an important role in training and educating Safari guides to improve knowledge and safety.
  
### **Fishing guide**

Guides employed by those seeking to fish for example [big game](https://en.wikipedia.org/wiki/Big-game_fishing) fishing in Sea, lakes and also rivers. Fishing guides have been important in many areas of the world, including the [Norwegian coast](https://en.wikipedia.org/wiki/Norwegian_language), the Swedish archipelago, Florida coast, and north Canada rivers and lakes, etc. Fishing guides in the Gulf of Mexico are commonly referred to as a Charter Guide. There are also specialists such as a [Fly fishing Guide](https://en.wikipedia.org/wiki/Fly_fishing_Guide). There are thousands of fishing guides but some are better than others. Often when searching on search engines like google, captains that pay for ad-services appear higher in priority (not necessarily when they are the best).

-----------------------------------------------------------------------

 

  
